const articlesTable = document.getElementById('articles_table');

if(articlesTable){
    articlesTable.addEventListener('click', e => {
        if(e.target.className === 'btn btn-danger delete-article'){
            if(confirm('Delete This Article?')){
                let id = e.target.getAttribute('data-id');
                fetch(`/articles/${id}`,{
                    method: 'DELETE'
                }).then(r => window.location.reload())
            }
        }
    })
}