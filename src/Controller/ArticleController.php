<?php

namespace App\Controller;

use App\Entity\Article;
use App\Form\ArticleFormType;
use App\Service\FileUploader;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ArticleController extends AbstractController
{
    /**
     * @Route("/articles", methods={"GET"}, name="articles_index")
     */
    public function index(Request $request)
    {
        $repo = $this->getDoctrine()->getRepository(Article::class);

        if ($request->query->get('filter') === "category-asc") {
            return $this->render("articles/index.html.twig", ['articles' => $repo->toggleCategorySort('category-asc')]);
        }
        if($request->query->get('filter') === "category-desc"){
            return $this->render("articles/index.html.twig", ['articles' => $repo->toggleCategorySort('category-desc')]);
        }
        return $this->render("articles/index.html.twig", ['articles' => $repo->latest()]);
    }

    /**
     * @Route("/article/new", methods={"GET","POST"}, name="articles_new")
     */
    public function new(Request $request, FileUploader $fileUploader)
    {
        $formBuilder = $this->createForm(ArticleFormType::class, $article = new Article());

        $formBuilder->handleRequest($request);

        if ($formBuilder->isSubmitted() && $formBuilder->isValid()) {

            $imageFile = $formBuilder->get('image')->getData();

            if ($imageFile) {
                $imageFileName = $fileUploader->upload($imageFile);
                $article->setImage($imageFileName);
            }

            $article = $formBuilder->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($article);
            $entityManager->flush();

            $this->addFlash('message', 'Article successfully created');

            return $this->redirectToRoute("articles_index");
        }
        return $this->render("articles/new.html.twig",
            ['formBuilder' => $formBuilder->createView()]
        );
    }

    /**
     * @Route("/articles/{id}", methods={"GET"}, name="articles_show")
     */
    public function show($id)
    {
        $article = $this->getDoctrine()->getRepository(Article::class)->find($id);
        return $this->render("articles/show.html.twig",
            ['article' => $article]);
    }

    /**
     * @Route("/articles/edit/{id}", methods={"POST","GET"}, name="articles_update")
     */
    public function update(Request $request, FileUploader $fileUploader, $id)
    {
        $article = $this->getDoctrine()->getRepository(Article::class)->find($id);
        $formBuilder = $this->createForm(ArticleFormType::class, $article);

        $formBuilder->handleRequest($request);
        if ($formBuilder->isSubmitted() && $formBuilder->isValid()) {
            $imageFile = $formBuilder->get('image')->getData();

            if ($imageFile) {
                $filesystem = new Filesystem();
                $filesystem->remove($fileUploader->getTargetDirectory().'/'.$article->getImage());
                $imageFileName = $fileUploader->upload($imageFile);
                $article->setImage($imageFileName);
            }

            $article = $formBuilder->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($article);
            $entityManager->flush();

            $this->addFlash('message', 'Article successfully edited');
            return $this->redirectToRoute("articles_index");
        }

        return $this->render("articles/edit.html.twig",
            [
                'formBuilder' => $formBuilder->createView(),
                'article' => $article,
            ]
        );
    }

    /**
     * @Route("/articles/{id}", methods={"DELETE"}, name="articles_destroy")
     */
    public function destroy($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $article = $entityManager->getRepository(Article::class)->find($id);

        $entityManager->remove($article);
        $entityManager->flush();

        $this->addFlash('message', 'Article successfully deleted');

        $response = new Response();
        $response->send();
    }
}