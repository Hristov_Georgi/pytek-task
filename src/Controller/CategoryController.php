<?php

namespace App\Controller;

use App\Entity\Category;
use App\Form\CategoryFormType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class CategoryController extends AbstractController
{
    /**
     * @Route("/categories", methods={"GET"}, name="categories_index")
     */
    public function index(): Response
    {
        $repo = $this->getDoctrine()->getRepository(Category::class);

        return $this->render('categories/index.html.twig', [
            'categories' => $repo->findAll(),
        ]);
    }

    /**
     * @Route("/categories/new", methods={"GET","POST"}, name="categories_new")
     */
    public function new(Request $request) : Response
    {
        $category = new Category();
        $formBuilder = $this->createForm(CategoryFormType::class,$category);
        $formBuilder->handleRequest($request);

        if($formBuilder->isSubmitted() && $formBuilder->isValid()){
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($category);
            $entityManager->flush();

            $this->addFlash('message','Category successfully created');
            return $this->redirectToRoute("categories_index");
        }
        return $this->render('categories/new.html.twig', ['formBuilder' => $formBuilder->createView() ]);     
    }

    /**
     * @Route("/categories/{id}", methods={"GET"}, name="categories_show")
     */ 
    public function show($id){
        $category = $this->getDoctrine()->getRepository(Category::class)->find($id);

        return $this->render('categories/show.html.twig', [
            'category' => $category,
            'categoryArticles' => $category->getArticles(),
        ]);
    }
}

