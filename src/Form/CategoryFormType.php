<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class CategoryFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => "Category Name",
                'attr' => array('class' => 'form-control')
            ])
            ->add('save', SubmitType::class, array(
                'label' => 'create',
                'attr' => array('class' => 'btn btn-primary mt-3')));
//        ->getForm();
    }

}