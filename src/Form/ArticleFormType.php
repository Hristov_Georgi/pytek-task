<?php

namespace App\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class ArticleFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'attr' => array('class' => 'form-control')))
            ->add('summary', TextType::class, array(
                'attr' => array('class' => 'form-control')))
            ->add('description', TextareaType::class, array(
                'attr' => array('class' => 'form-control')))
            ->add('category', EntityType::class, [
                'placeholder' => 'Choose Category',
                'class' => 'App\Entity\Category',
                'label' => "Select Category",
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
            ->add('image', FileType::class, array(
                'label' => 'Image (Optional)',
                'required' => false,
                'mapped' => false,
                'attr' => array('class' => 'form-control')))
            ->add('save', SubmitType::class, array(
                'label' => 'create',
                'attr' => array('class' => 'btn btn-primary mt-3')));
    }
}