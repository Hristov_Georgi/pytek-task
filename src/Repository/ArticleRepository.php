<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }

    public function latest() : array
    {
        $entityMangager = $this->getEntityManager();
        $query = $entityMangager->createQuery(
           'SELECT a
            FROM App\Entity\Article a
            ORDER BY a.createdAt DESC'
        );

        return $query->getResult();
    }

    public function toggleCategorySort($toggler){
        if ($toggler === 'category-asc'){
            return $this->sortByCategoryDesc();
        }
        return $this->sortByCategoryAsc();
    }

    private function sortByCategoryDesc() : array
    {
        $entityMangager = $this->getEntityManager();
        $query = $entityMangager->createQuery(
            // raw sql query: select * from article order by category_id DESC
            'SELECT a
            FROM App\Entity\Article a
            ORDER BY a.category DESC'
        );

        return $query->getResult();
    }
    private function sortByCategoryAsc() : array
    {
        $entityMangager = $this->getEntityManager();
        $query = $entityMangager->createQuery(
        // raw sql query: select * from article order by category_id DESC
            'SELECT a
            FROM App\Entity\Article a
            ORDER BY a.category ASC'
        );

        return $query->getResult();
    }
}
