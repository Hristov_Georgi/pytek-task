#About The Project

Pytek Article Task

Requirements
 Create a small project for maintaining Articles and Article Categories - with just a basic html and 
 no authentication required.
 - Category has a name, Article has a name, category, image, summary and description;
 - Have two forms - for entering new Categories and Articles;
 - Have a view for articles with a filter - to list articles by category. Articles are sorted by date,
 with information for name and category and a link to detail page;
 - Detail Artiicle page shows all the available article information with a back to list link.


Build With:
1. Symfony
2. Bootstrap
3. Javascript

Framework: Symfony 4.4
 
#Installation

1. Install Dependencies: composer install
2. Create and Setup your Database. If using docker run: docker-compose up
3. Update Schema By: symfony console doctrine:schema:update --force
4. Start Server: symfony serve


